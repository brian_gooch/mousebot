#include <Servo.h>
#include "Mousemotors.h"

const byte ledPin = 13;
const byte buttonPin = 9;

Mousemotors motors;
void setup()
{
  // put your setup code here, to run once:
  motors.attach(6, 5);

  pinMode(ledPin, OUTPUT);
  pinMode(buttonPin, INPUT_PULLUP);

  while (digitalRead(buttonPin));
  {
  }


  motors.pollygon(3);
  delay(1000);
  motors.pollygon(4);
}

void loop()
{
  // put your main code here, to run repeatedly:
  digitalWrite(ledPin, HIGH);
}